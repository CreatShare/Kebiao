!(function( R, RD, $, d, $$, browser ){
    'use strict';

    let

		/* ==================== home主页 ================== */
    /**
     * 页面第一次加载现实的课表，由服务端填充到loaddata中
     */
    keyword = location.search.match(/k=(.*)$/),
    
    Head = R.createClass({
    		'login': function(){
    				$(this.refs.login).show()
    													.children('p').text('');
    		},
    		'cancel': function(){
    				$(this.refs.login).hide().children('input').val('');
    		},
    		'logout': function(){
    				let that = this;
    				
    				$.ajax({
    						'url': 'https://corefuture.cn/creatshare_zhjs/user/logout.action',
                'crossDomain': true,
								'xhrFields': { 
										withCredentials: true 
								},
    						'type': 'POST',
    						'success': function(data, textStatus, request){
    								if(data.total === -3){
						    				sessionStorage.removeItem('userid');
						    				sessionStorage.removeItem('username');
						    				
						    				$(that.refs.libtn).show().next().hide();
    								}else{
    										$(that.refs.login).show().children('p').text(data.total+''+data.Msg);
    								}
    						}
    				});
    		},
    		'checkRecord': function(){
    			let userid = sessionStorage.getItem('userid');
    			if(userid){
    					if($('#lastuser').attr('data-userid') !== userid){
	    					// 初始化刷卡记录页面
	    					RD.render( <Back />, d.getElementById('backing'));
								RD.render( <UserRecord />, d.getElementById('history'));
							}
							
							// 隐藏主页面，显示刷卡记录
							$('section#home').hide().next().show();
    			}else{
    					$(this.refs.login).show().children('p').text('请登录后查看刷卡记录！');
    			}
    		},
    		'showUser': function(){
    				let refs = this.refs;
						$(refs.libtn).hide();
						
						$(refs.login).hide();
						$(refs.logout).show().children('#username').text(sessionStorage.getItem('username'));      		
    		},
    		'componentDidMount': function(){
    				let ele = this.refs.login,
    						that = this;
    				$('body').click(function(e){
    					let tar = e.target;
    					if($(ele).css('display') === 'none' || tar.parentNode === ele || tar === ele) return;
    					that.cancel();
    				});
    				
    				if(sessionStorage.getItem('userid'))
    					this.showUser();
    		},
    		'userLogin': function(){
    				let refs = this.refs,
    						tip = refs.tip,
    						login = $(refs.login),
    						userid = login.children('input[type="text"]').val().trim(),
    						pass = login.children('input[type="password"]').val().trim();
      			if(userid.length !== 8 || !(/^\+?[0-9]*$/.test(userid))){
      					tip.innerText = '学号输入有误！';
      					return;
      			}else if(!pass.length){
      					tip.innerText = '密码不得为空！';
      					return;
      			}
      			tip.innerText = '登录中……';
      			
      			let that = this;
      			let d = $.ajax({
      					'url': 'https://corefuture.cn/creatshare_zhjs/user/login.action',
      					'type': 'POST',
                'crossDomain': true,
								'xhrFields': { 
										withCredentials: true 
								},
      					'dataType': 'json',
      					'data': {'userName':userid,'userPassword':pass},
      					'success': function(data, textStatus, request){
      						if(data.IsSucceed){
      							sessionStorage.setItem('userid', userid);
      							sessionStorage.setItem('username', data.Obj.NAME);
      							
      							that.showUser();
      						}else{
      							tip.innerText = data.Msg;
      						}
      					}
      			});
    		},
    		'render':	function(){
    				return (
    					<div>
				        <button className="history" onClick={ this.checkRecord }>查看刷卡记录</button>
				        <button className="login" ref="libtn" onClick={ this.login }>登录</button>
				        <div className="logoutpage" ref="logout">
					        <span id="username"></span>
				        	<a onClick={ this.logout }>注销</a>
				        </div>
				        <div className="loginpage" ref="login">
				        	<span>学号</span><input type="text" placeholder="请输入八位学号"/>
				        	<span>密码</span><input type="password" placeholder="请输入一卡通密码"/>
				        	<p ref="tip"></p>
				        	<button className="loginbtn" onClick={ this.userLogin }>登录</button>
				        	<button className="canclebtn" onClick={ this.cancel }>取消</button>
				        </div>
				      </div>   				
    				);
    		}
    }),
    
    /**
     * SearchSuggest组件用于用户输入错误信息后显示服务端的返回信息
     * @props data 组件接受一个名为data的props，data为服务端的错误信息响应值
       {
           "code": 3005,
           "content": "请求数据不合法！"
       }
     */
    SearchSuggest = React.createClass({
        'handleData': function( data ){
            let res = {};

            if( data.code !== 3001 ){
                res.info = data.content;
            }else{
                res.info = '专业名称有误，下列为建议名称，点击可查询。';
            }

            res.list = data.major ? data.major : [] ;

            return res;
        },
        // 点击建议列表触发事件
        'applySugg': function( e ){
            let value = e.target.textContent;
            $( '.tool input ').trigger('applySugg', value);
        },
        'render': function(){
            let data = this.handleData( this.props.data );
            return (
                <div className="suggest">
                    <ul>
                        <li>{data.info}</li>
                        {
                            data.list.map(function( ele, i ){
                                return <li key={i} onTouchStart={this.applySugg} onClick={this.applySugg}>{ele}</li>
                            }.bind(this))
                        }
                    </ul>
                </div>
            )
        }
    }),

    /**
     * SearchPanel组件包涵搜索输入框和搜索类型select
     * 无需接受props
     */
    SearchPanel = React.createClass({
        'getInitialState': function(){
            return {
                'placeholder': '例如:  计科1601',
                'searchtype': 'class',
                'ajaxQueue': {},
                'loadbarQueue': {}
            }
        },
        // 挂载组建后监听applySugg事件，用于快速应用专业建议
        'componentDidMount': function(){
            if( keyword ){
                let v = decodeURIComponent(keyword[1]);
                $( this.refs.se ).val( v );
                this.handleInput({target:{value:v}});
                keyword = '';
            }
            $( this.refs.se ).on('applySugg', function( e ){
                let newKW = this.state.keyword.replace( /^[\u4E00-\u9FFF]*/, e._args);

                // 手动更新输入框，重新查询
                $( '.tool input' ).val( newKW );
                this.handleInput({target:{value:newKW}});
            }.bind(this))
        },
        // 处理搜索类型选择的select
        'handleSelect': function( e ){
            let value = e.target.value;
            switch( value ){
                case '班级':
                    this.setState({'placeholder':'例如：计科1601'});
                    this.state.searchtype = 'class';
                    break;
                case '教师':
                    this.setState({'placeholder':'例如：赵小强'});
                    this.state.searchtype = 'teacher';
                    break;
                case '课程':
                    this.setState({'placeholder':'例如：伦理学'});
                    this.state.searchtype = 'course';
                    break;
            }
            this.clearInputValue();
        },
        // 发ajax
        'Queue': function( searchtype, value ){
            return $.ajax({
                'url': `http://nwx.changxiaoyuan.com/index.php/schedule/search/outputData/?model=${searchtype}&value1=${value}&value2=0&ajax=_`,
                'dataType': 'json',
                'beforeSend': function( xhr ){
                    xhr.addEventListener('readystatechange', function(){
                        let width = 100/(5 - xhr.readyState);
                        ReactDOM.render( <LoadBar width={width}/>, d.getElementById( 'loadbar' ) );
                    });
                },
                'success': function( data ){
                    // 不是2000说明输入有误，显示用户建议组件
                    if( data.code !== 2000 ){
                        ReactDOM.render( <SearchSuggest data={data} />, d.getElementById( 'sugg' ) );
                        return false;
                    }
                    if( searchtype === 'class'){
                        // 用户输入正确，显示用户课程及更新页面提示
                        let info = `${data.schedule[0].major}${data.schedule[0].class}`;

                        ReactDOM.render( <Usertip text={info} title={true} type="class"/>, d.getElementById( 'usertip' ) );
                        ReactDOM.render( <ClassBlock allClass={data}/>, d.getElementById( 'classes' ) );
                    }else if( searchtype === 'teacher'){
                        // 用户输入正确，显示用户课程及更新页面提示
                        ReactDOM.render( <div></div>, d.getElementById( 'usertip' ) );
                        ReactDOM.render( <ContentBlock type={'teacher'} allClass={data} title={value}/>, d.getElementById( 'classes' ) );
                    }else{
                        ReactDOM.render( <div></div>, d.getElementById( 'usertip' ) );
                        ReactDOM.render( <ContentBlock type={'course'} allClass={data} title={value}/>, d.getElementById( 'classes' ) );
                    }
                }
            })
        },
        // 发ajax前处理数据，由handleInput调用
        'QueueHandle': function( value ){

            // 没有用户输入则不干任何事
            if( !value ) return false;

            let match = value.match( /^([\u4E00-\u9FFF]{2,7})(\d{4})$/gi ),
                searchtype = this.state.searchtype;

            // 移除搜索结果建议框
            ReactDOM.render( <div></div>, d.getElementById( 'sugg' ) );

            // 对不同搜索类型进行不同的参数处理
            if(searchtype === 'class'){
                // 匹配到类似 广电1202 的字符串才发请求
                if( match === null ) return false;
            }else if( searchtype === 'course' ){
                value = value.replace(/#/, '%23')
                             .replace(/&/, '%26');
            }

            // 取消上一个ajax
            this.state.ajaxQueue.abort && this.state.ajaxQueue.abort();

            this.state.ajaxQueue = this.Queue( searchtype, value );

        },
        'handleInput': function( e ){

            let value = e.target.value.trim();

            this.state.keyword = value;

            this.QueueHandle( value );
        },
        'clearInputValue': function(){
            this.state.keyword = '';
            $( this.refs.se ).val( '' );
        },
        'render': function(){
            let placeholder = this.state.placeholder;
            return(
                <div className="tool">
                    <input type="text" autofocus="autofocus" maxLength="20"
                    placeholder={placeholder} onChange={this.handleInput} ref="se"/>

                    <select onChange={this.handleSelect}>
                        <option>班级</option>
                        <option>教师</option>
                        <option>课程</option>
                    </select>

                    <div id="sugg"></div>
                </div>
            )
        }
    }),

    /**
     * ClassList组件用于显示每一节课，
     * @props  className  课程名
     * @props  classTime  课程时间
     * @props  classTeacherName  课程教师名
     * @props  classPosition  课程教室名
     * @props  classNth  课程顺序 1,2,3,4,5
     * @props  classPassed  控制这门课是否显示为今日已上过的class
     * @props  classOn  控制显示当前是否正在上课的标示s
     */
    ClassList = React.createClass({
        'render': function(){
            let className = this.props.className || '无',
                classTime = className === '无' ? '' : this.props.classTime,
                classTeacherName = this.props.classTeacherName,
                classPosition = this.props.classPosition,
                classNth = this.props.classNth,
                classPassed = this.props.classPassed,
                classOn = this.props.classOn;

            return (
                <li className={`${classPassed} ${classOn}`}>
                    <span className="class-nth">{classNth}</span>
                    <p className="class-name">{className}</p>
                    <span className="class-label">{classTime}</span>
                    <span className="class-label">{classTeacherName}</span>
                    <span className="class-label class-posi">{classPosition}</span>
                </li>
            )
        }
    }),

    /**
     * ClassEach组件用于显示每一日的所有课程
     * @props  thisClass 一个对象，包含一日的所有课程
     * {
            'week': '四',
            'thisClass': {
                ...
            }
       }
     */
    ClassEach = React.createClass({
        'handleData': function( data ){

            let classArr;

            if( data.type === 'class' ){
                classArr = [{
                    'className': data.thisClass.first_course,
                    'classTeacherName' : data.thisClass.first_teacher,
                    'classTime': $$.getTimeStr( '1' ),
                    'classPosition': data.thisClass.first_room,
                    'classNth': '1',
                    'classPassed': $$.isPassed( data.thisClass.week, $$.getTimeStr( '1' ) ),
                    'classOn': $$.onClass( data.thisClass.week, $$.getTimeStr( '1' ) )
                },{
                    'className': data.thisClass.second_course,
                    'classTeacherName' : data.thisClass.second_teacher,
                    'classTime': $$.getTimeStr( '2' ),
                    'classPosition': data.thisClass.second_room,
                    'classNth': '2',
                    'classPassed': $$.isPassed( data.thisClass.week, $$.getTimeStr( '2' ) ),
                    'classOn': $$.onClass( data.thisClass.week, $$.getTimeStr( '2' ) )
                },{
                    'className': data.thisClass.third_course,
                    'classTeacherName' : data.thisClass.third_teacher,
                    'classTime': $$.getTimeStr( '3' ),
                    'classPosition': data.thisClass.third_room,
                    'classNth': '3',
                    'classPassed': $$.isPassed( data.thisClass.week, $$.getTimeStr( '3' ) ),
                    'classOn': $$.onClass( data.thisClass.week, $$.getTimeStr( '3' ) )
                },{
                    'className': data.thisClass.fourth_course,
                    'classTeacherName' : data.thisClass.fourth_teacher,
                    'classTime': $$.getTimeStr( '4' ),
                    'classPosition': data.thisClass.fourth_room,
                    'classNth': '4',
                    'classPassed': $$.isPassed( data.thisClass.week, $$.getTimeStr( '4' ) ),
                    'classOn': $$.onClass( data.thisClass.week, $$.getTimeStr( '4' ) )
                }]
            }else if( data.type === 'teacher'){
                classArr = [{
                    'className': data.thisClass.course,
                    'classTeacherName' : data.thisClass.teacher,
                    'classTime': $$.getTimeStr( data.thisClass.section ),
                    'classPosition': data.thisClass.room,
                    'classNth': data.thisClass.section,
                    'classPassed': '',
                    'classOn': ''
                }]
            }

            return {
                week: data.thisClass.week,
                weekNum: data.thisWeekNum,
                classArr: classArr
            }
        },
        'showTip': function getData(e){
        	var ele = $('#usertip #tip'),
        			arr = e.target.id.split('_'),
        			xindex = Math.floor(+arr[0]/2),
        			yindex = +arr[1],
        			array = this.props.anotherdata[xindex][yindex];
        	arr = array.split('_');
        	ele.children()[0].innerText = '任课老师：' + arr[0];
        	ele.children()[1].innerText = '上课教室：' + arr[1];
        	ele.children()[2].innerText = '上课时间：' + ((xindex === 0) ? '08:00-09:45' : 
        																								(xindex === 1) ? '10:15-12:00' : 
        																								(xindex === 2) ? '14:30-16:15' : '16:35-18:20');
        	var topoffset = ($(window).height() - ele.height())/2 + d.body.scrollTop;
        	ele.show().css('top', topoffset);
        },
        'render': function(){
            let data = this.handleData( this.props ),
            		num = this.props.no;
           	
           	if(this.props.type === 'class'){
           			let arr = this.props.thisClass;
           			return (
           					<tr>
           						<td>{num}</td>
           						{
				        				arr.map((function(e, i){
					        				return (e === null) ? ( <td key={ i }></td> ) : 
					        															( <td key={ i } rowSpan="2"><span id={ `${num}_${i}` } onClick={ this.showTip }>{e}</span></td> );
					        			}).bind(this))
			        				}
           					</tr>
           			);
           	}
           	
            return (
                <section className="class" id={`week${data.weekNum}`}>
                    <h2>星期{data.week}</h2>
                    <ul>
                        {
                            data.classArr.map(function( ele, i ){
                                return (
                                    <ClassList key={i}
                                               className={ele.className}
                                               classTeacherName={ele.classTeacherName}
                                               classTime={ele.classTime}
                                               classPosition={ele.classPosition}
                                               classNth={ele.classNth}
                                               classPassed={ele.classPassed}
                                               classOn={ele.classOn} />
                                )
                            })
                        }
                    </ul>
                </section>
            )
        }
    }),

    /**
     * ClassBlock组件用于显示所有课程
     * @props allClass 包涵所有待显示的所有日的所有课程
     * {
        "code": 2000,
        "schedule": {
            ...
            }
        }

        {
            "code": 2000,
            "schedule": [
                {
                    ...
                },
                {
                    ...
                }
            ]
        }
     * @props type 组件显示的数据类型： class. teacher
     */
    ClassBlock = React.createClass({
        'handleData': function( data ){
            let classes = data.allClass.schedule;
            return {
                'classes': !classes.length ? [classes] : classes
            }
        },
        'dealData': function(arr){
        	var res = [[], [], [], [], [], [], [], []];
        	var cur;
        	for(var i = 0; i < 5; i++){
        		cur = arr[i];
        		res[0].push(cur['first_course'] ? cur['first_course']+cur['first_room'] : null);	
        		res[2].push(cur['second_course'] ? cur['second_course']+cur['second_room'] : null);	
        		res[4].push(cur['third_course'] ? cur['third_course']+cur['third_room'] : null);	
        		res[6].push(cur['fourth_course'] ? cur['fourth_course']+cur['fourth_room'] : null);	
        	}
        	return res;
        },
        'getMoreData': function getMoreData(arr){
        		var res = [[], [], [], []];
        		var cur;
        		for(var i = 0; i < 5; i++){
        			cur	= arr[i];
        			res[0].push(cur['first_course'] ? cur['first_teacher']+'_'+cur['first_room'] : null);
        			res[1].push(cur['second_course'] ? cur['second_teacher']+'_'+cur['second_room'] : null);
        			res[2].push(cur['third_course'] ? cur['third_teacher']+'_'+cur['third_room'] : null);
        			res[3].push(cur['fourth_course'] ? cur['fourth_teacher']+'_'+cur['fourth_room'] : null);
        		}
        		return res;
        },
        // 组件挂载后显示当前星期的课程
        'componentDidMount': function(){
            ReactDOM.render( <ScrollToView/>, d.getElementById( 'scrollto' ) );
            
            $('body').click(function(e){
        			var cur = e.target,
        					ele = $('#usertip #tip');	
        			if(cur.tagName !== 'SPAN' ||(/[1, 3, 5, 7]_[0-4]/.test(cur.id) === false && ele.css('visibility') === 'visible'))
        			ele.hide();
        		});
        },
        'componentWillUpdate': function(){
            ReactDOM.render( <ScrollToView/>, d.getElementById( 'scrollto' ) );
        },
        'render': function(){
            let data = this.handleData( this.props ),
            		d = this.dealData( data.classes ),
            		another = this.getMoreData( data.classes );
            return (
                <table className="table" cellSpacing="0">
            			<thead>
            				<tr>
            					<td>节数</td>
            					<td>周一</td>
            					<td>周二</td>
            					<td>周三</td>
            					<td>周四</td>
            					<td>周五</td>
            				</tr>
            			</thead>
            			<tbody>
            				{
                        d.map(function( e, i ){
                            return <ClassEach type={'class'} key={i}
                                    thisClass={e} no={`${i+1}`} anotherdata={another} />
                        })
                    }
            			</tbody>
            		</table>
            )
        }
    }),

    /**
     * ContentBlock组件用于显示教师搜索结果
     * @props allClass  包涵所有待显示的所有日的所有课程, 同ClassEach
     * @props title 用于显示页面title，这里显示为搜索关键字
     * @props type 用于表明组件调用类型
     */
    ContentBlock = React.createClass({
        'handleData': function( data ){
            let dict = {},
                res = [],
                title = data.title,
                type = data.type;
            data.allClass.schedule.forEach(function( ele, i ){
                if( !dict[ele.teacher] ){
                    dict[ele.teacher] = [];
                }
                dict[ele.teacher].push( ele );
            });
            for(let i in dict){
                res.push({
                    'teacherName': i,
                    'classes': dict[i]
                })
            }
            return {title, res, type};
        },
        'render': function(){
            let data = this.handleData( this.props );
            return (
                <div>
                    {
                        data.res.map(function( ele, i ){
                            return <AgentEach thisTeacher={ele} title={data.title} key={i} type={data.type} />
                        })
                    }
                </div>
            )
        }
    }),

    /**
     * AgentEach组件用于显示一个教师及她所带的课，它的存在是用于包装ClassEach
     */
    AgentEach = React.createClass({
        'render': function(){
            let data = this.props,
                info;
            if( data.type === 'teacher' ){
                info = <Usertip text={data.thisTeacher.teacherName} title={data.title} type="teacher"/>;
            }else{
                info = '';
            }
            return (
                <div>
                    {info}
                    {
                        data.thisTeacher.classes.map(function( ele, i ){
                            return <ClassEach type={'teacher'} thisClass={ele} key={i} />
                        })
                    }
                </div>
            )
        }
    }),

    /**
     * Usertip组件用于显示当前页面是哪个专业的课，并且更新页面title
     * @props text 包含待显示的数据
     * @props title 当为true时，与text显示相同数据，否则显示传入数据
     */
    Usertip = React.createClass({
        'handleData': function( data ){
            let infoText = data.text,
            		type = data.type,
                titleText = data.title === true ? infoText : data.title;
            titleText = `${titleText}-畅校园课表`;
            return {infoText, type, titleText};
        },
        'render': function(){
            let data = this.handleData( this.props );

            // 显示用户提示的时候同时更新页面title
            if( d.title !== data.titleText ){
                d.title = data.titleText;
            }
            
            if(data.type === 'teacher'){
            		return (
		            		<div className="usertip">
		                    <h1>{data.infoText}</h1>
		                </div>
            		);
            }
            
            return (
                <div className="usertip">
                    <div className="tip" id="tip">
                    	<p></p>
                    	<p></p>
                    	<p></p>
                    </div>
                </div>
            )
        }
    }),

    /**
     * LoadBar组件用于显示进度条
     * @props width 进度条宽度百分比
     */
    LoadBar = React.createClass({
        'getInitialState': function(){
            return {'remove':{}}
        },
        'componentDidUpdate': function(){
            if( this.props.width === 100 ){
                this.willRemove();
            }
        },
        'componentWillUnmount': function(){
            clearTimeout( this.state.remove );
        },
        'willRemove': function(){
            this.state.remove = setTimeout(function(){
                ReactDOM.render( <div></div>, d.getElementById( 'loadbar' ) );
            }, 1300);
        },
        'render': function(){
            let width = this.props.width,
                ishide = width === 100 ? 'loadbar loadbar-hide' : 'loadbar';
            return <div ref="loadbar" className={ishide} style={{width: width + '%'}}></div>
        }
    }),

    /**
     * DefauluInfo用于无服务端填充数据时的默认数据显示
     */
    DefauluInfo = React.createClass({
        'render': function(){
            return (
                <p className="defaultinfo">快速搜索课程、老师及班级信息<br/>畅校园课程表<br/>
                		<span className="defaultinfotip">由于iPhone在微信平台内核不同，导致查看刷卡记录和注销异常。小畅正努力修复中………</span></p>
            )
        }
    }),

    ScrollToView = React.createClass({
        'getInitialState': function(){
            return {'target':{}}
        },
        'scrollTo': function(){
            let target = this.state.target;
            if( requestAnimationFrame && target.getBoundingClientRect ){
                let targetPosi = d.body.scrollTop + target.getBoundingClientRect().top,
                base = d.body.scrollTop,
                diff = 5;
                function scrollToWeek(){
                    base = base + ( diff += 4 ) > targetPosi ? targetPosi : base + ( diff += 4 );
                    scrollTo( 0, base );
                    if( base === targetPosi ){
                        return false;
                    }else{
                        requestAnimationFrame( scrollToWeek );
                    }
                }
                requestAnimationFrame( scrollToWeek );
            }else{
                target.scrollIntoView();
            }
        },
        'render': function(){
            let target = d.getElementById( `week${new Date().getDay()}` ),
                renderDom;
            this.state.target = target;
            if( target ){
                renderDom = <p className="scrollto" onTouchStart={this.scrollTo} onClick={this.scrollTo}>查看今日课程</p>
            }else{
                renderDom = <span></span>
            }

            return renderDom;
        }
    }),
    
    /* ==================== records页面 ================== */
    /**
     * Back组件包含返回按钮
     * 无需接受props
     */
		Back = R.createClass({
			// 检测是否登陆
			'componentDidMount': function(){
					if(!sessionStorage.userid || !sessionStorage.username)
						$(this.refs.errtip).text('不正常访问，请返回主页登录！').show();
			},
			// 返回home主页面
			'backhome': function(){
					$('section#home').show().next().hide();
			},
			'render': function(){
					return (
						<div className="head">
							<button onClick={ this.backhome }>返回</button>
							<span ref="errtip" id="errtip"></span>
						</div>
					);
			}
		}),
		
		/**
     * CardHistory组件：用户某一次历史刷卡异常记录
     * @props  record  一条异常记录
     * @props  no  异常记录的唯一序号
     */
		CardHistory = R.createClass({
			// 点击申诉按钮
			'appeal': function(e){
      	$('#apply').attr('data-cur', $(e.target).attr('data-no')).show() 
      							.children('p[data-type="errinfo"]').text('');
      },
			'render': function(){
				let props = this.props,
						record = props.record,
						no = props.no,
						info = record.Class_No + '_'
									+ record.WaterDate + '_'
									+ record.JT_No + '_'
									+ record.RBH + '_'
									+ record.Term_No + '_'
									+ record.Status + '_'
									+ record.SBH;
				return (
					<div className="record" data-info={ info }>
						<p className="">{ record.WaterDate }</p>
						<span className="course">{ record.S_Name }</span>
						<span className="state">{ record.Status === 3 ? '缺勤' : '迟到'}</span>
						<span className="appeal" data-no={no} onClick={ this.appeal }>申诉</span>
					</div>
				);	
			}
		}),
		
		/**
     * ExceptionCourses组件: 用户历史刷卡异常记录
     * @props  courses  异常记录
     */
		ExceptionCourses = R.createClass({
			'render': function(){
					let courses = this.props.courses;
					return (
						<div className="exception">
								{
										courses.map(function(e, i){
												return <CardHistory record={e} key={i} no={i}/>;
										})
								}
						</div>
					);	
			}
		}),
			
		/**
     * UserRecord组件包涵用户名和用户历史刷卡记录
     * 无需接受props
     */
    UserRecord = R.createClass({
        'componentDidMount': function(){
        		this.Queue();
        		
        		let ele = this.refs.a;
            $('body').click(function(e){
        			let tar = e.target;
    					if($(ele).css('display') === 'none' || tar.parentNode === ele || tar === ele || tar.className === 'sub') return;
        			$(ele).hide();
        		});
        },
        'componentDidUpdate': function(){
        		this.refs.c.scrollTop = 0;
        },
        'apply': function(){
        		let contentele = this.refs.a,
        				errele = $(contentele).children('p'),
        				reason = $(contentele).children('textarea').val().trim();
        				
        		if(reason.length <= 5){
        				errele.text('错误：申诉理由不得少于5个字！');
        				return ;
        		}
        		
        		let status = $('.apply input[type="radio"]:checked').val();
        		if(!status){
        				errele.text('错误：请选择一个状态！');
        				return ;
        		}
        		
        		let no = +$(this.refs.a).attr('data-cur'),
        				data = $($('.record')[no]).attr('data-info').split('_');
        				
        		$.ajax({
        				'url': 'https://corefuture.cn/creatshare_zhjs/attend/appeal.action',
        				'type': 'POST',
        				'dataType': 'json',
                'crossDomain': true,
								'xhrFields': { 
										withCredentials: true 
								},
        				'data': {
        					'class_No': data[0],
        					's_Date': data[1],
        					'jc': data[2],
        					'r_BH': data[3],
        					'term': data[4],
        					'remark': reason,
        					's_Status': data[5],
        					'a_Status': status,
        					's_Code': data[6]
        				},
        				'success': function(data){
        				
        						errele.text('提交中……');
        						
        						if(data.total && data.total === -2){
        							$(contentele).hide();
        							$('#errtip').text('不正常访问，请返回主页登录！').show();
        							
        							return;
        						}
        						if(!data.IsSucceed){
        							errele.text('错误：已申诉，不可重复申诉！');
        							return;
        						}
        						
        						$(contentele).hide();
        				}
        		});
        },
        'cancel': function(){
        		$(this.refs.a).hide().children('textarea').val('');
        },
        // 发ajax
        'Queue': function Queue() {
        		var that = this;
        		return $.ajax({
                'url': 'https://corefuture.cn/creatshare_zhjs/attend/getMonthAttend.action',
                'type': 'POST',
                'dataType': 'json',
                'crossDomain': true,
								'xhrFields': { 
										withCredentials: true 
								},
                'data': {
                	'rows': 80,
                	'page': 1,
                	'flag[0]': 2,
                	'flag[2]': 3
                },
                'success': function success(data) {
                		if(data.total <= 0){
                			RD.render(<h3>你的刷卡记录正常！<br/>（iPhone手机使用该功能异常）</h3>, that.refs.c);
                			return;
                		}
                		
                		RD.render(<ExceptionCourses courses={ data.rows }/>, that.refs.c);
                }
            });
        },
        'render': function render() {        		
        		return (
        			<div className="history">
        				<h2 id="lastuser" data-userid={ sessionStorage.getItem('userid') }>
        					{sessionStorage.getItem('username') || '无名氏'}的刷卡记录
        				</h2>
        				<div className="content" ref="c" id="c">
        				</div>
        				<div className="apply" ref="a" id="apply" data-cur="undefined">
        						<textarea placeholder="请写出申诉理由(不少于5个字)"></textarea>
        						<span>状态</span>
        						<input type="radio" id="statu_1" name="st" value="1"/><label htmlFor="statu_1">正常</label>
        						<input type="radio" id="statu_2" name="st" value="2"/><label htmlFor="statu_2">迟到</label>
        						<input type="radio" id="statu_3" name="st" value="3"/><label htmlFor="statu_3">请假</label>
        						<p data-type="errinfo"></p>
        						<div className="btngroup">
        							<button className="sub" onClick={ this.apply }>提交</button>
        							<button className="can" onClick={ this.cancel }>取消</button>
        						</div>
        				</div>
        			</div>
        		);
        }
    });
	
		// home主页
		RD.render( <Head/>, d.getElementById('header') );
    RD.render( <SearchPanel/>, d.getElementById( 'tool' ) );
    RD.render( <LoadBar width={0}/>, d.getElementById( 'loadbar' ) );

    RD.render( <DefauluInfo />, d.getElementById( 'classes' ) );

})( React, ReactDOM, Zepto, document , {

    /**
     * 把中文周数转为数字，分割每节课时间段
     * @param  {[str]} week 中文周数
     * @param  {[str]} time 课程时间段字符串
     * @param  {[object]} curTime 时间对象
     */
    'parseTime': function( week, time, curTime ){
        let weekNum,
            timeMatch = time.match(/^(\d{2}):(\d{2}) - (\d{2}):(\d{2})/);

        switch( week ){
            case '一':
                weekNum = 1;
                break;
            case '二':
                weekNum = 2;
                break;
            case '三':
                weekNum = 3;
                break;
            case '四':
                weekNum = 4;
                break;
            case '五':
                weekNum = 5;
                break;
        }

        let curTimeNum = +`${curTime.getHours()}${[''+curTime.getMinutes()].map(e=>e.length>1?e:'0'+e)[0]}`,
            before = +`${timeMatch[1]}${timeMatch[2]}`,
            after = +`${timeMatch[3]}${timeMatch[4]}`;

        return { weekNum, before, after, curTimeNum }
    },

    /**
     * 判断当前课程是否已经上过，基于本地时间判断
     * @param  {[str]} week 中文周数
     * @param  {[str]} time 课程时间段字符串
     * @return {[str]} 控制这门课是否显示为今日已上过的class
     */
    'isPassed': function( week, time ){

        let curTime = new Date(),
            targetTime = this.parseTime( week, time, curTime );

        if( curTime.getDay() !== targetTime.weekNum ){
            return ''
        }

        if( targetTime.curTimeNum > targetTime.after ){
            return 'cpd-true';
        }else{
            return ''
        }

    },

    /**
     * 判断是否在上某节课
     * @param  {[str]} week 中文周数
     * @param  {[str]} time 课程时间段字符串
     * @return {[str]} 控制是否高亮显示的css
     */
    'onClass': function( week, time ){
        let curTime = new Date(),
            targetTime = this.parseTime( week, time, curTime );

        if( curTime.getDay() !== targetTime.weekNum ){
            return '';
        }

        if( targetTime.before <= targetTime.curTimeNum
            && targetTime.curTimeNum <= targetTime.after){
            return 'cpd-on'
        }else{
            return ''
        }

    },

    /**
     * 根据大课节数返回上课时间段
     * @param  {[str]} section 大课节数
     * @return {[str]} 时间段字符串
     */
    'getTimeStr': function( section ){
        let time;
        switch( section ){
            case '1':
                time = '08:00 - 09:45';
                break;
            case '2':
                time = '10:15 - 12:00';
                break;
            case '3':
                time = '14:30 - 16:15';
                break;
            case '4':
                time = '16:45 - 18:20';
                break;
        }
        return time;
    }

}, {
		versions: function() {
			var u = navigator.userAgent, app = navigator.appVersion;
			
			return {//移动终端浏览器版本信息 
				trident: u.indexOf('Trident') > -1, //IE内核
				presto: u.indexOf('Presto') > -1, //opera内核
				webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
				gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1, //火狐内核
				mobile: !!u.match(/AppleWebKit.*Mobile.*/) || !!u.match(/AppleWebKit/), //是否为移动终端
				ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
				android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, //android终端或者uc浏览器
				iPhone: u.indexOf('iPhone') > -1 || u.indexOf('Mac') > -1, //是否为iPhone或者QQHD浏览器
				iPad: u.indexOf('iPad') > -1, //是否iPad
				webApp: u.indexOf('Safari') == -1 //是否web应该程序，没有头部与底部
			};
		}(),
		
		language: (navigator.browserLanguage || navigator.language).toLowerCase()
});
