# CreatShare-Kebiao

## 畅校园微信平台课表

请大家在开发的时候先fork，在fork的库上更改之后向主库提pr，提之前最好rebase一下，避免冲突。

版本:

* V1.0

### 项目依赖：

* React
* gulp
* sass
* livereload
* Bebel

### 开发环境

1. 从 FD/Kebiao Fork 后，git clone 你的fork库地址。
2. ``` yarn ``` ，安装项目依赖。
3. ``` npm run dev ``` ，起一个本地server，端口8000，livereload + Sass + ES6 自动编译。

### 线上环境

1. 从 FD/Kebiao 直接 clone 项目到本地。
2. 运行 ``` yarn ``` ，安装项目依赖。
2. 运行 ``` npm run build ``` ，生成可部署的所有资源，输出为当前文件夹的 ``` dest ``` 目录。
