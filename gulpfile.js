'use strict'

let gulp = require( 'gulp' ),
    sass = require( 'gulp-sass' ),
    server = require( 'gulp-server-livereload' ),
    sourcemaps = require( 'gulp-sourcemaps' ),
    babel = require( 'gulp-babel' ),
    rename = require( 'gulp-rename' ),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    htmlreplace = require('gulp-html-replace');

gulp.task('default', ['es6', 'css', 'server'] , function() {
    gulp.watch(['public/css/style.scss', 'public/css/common.scss'], ['css'])
        .on('change', function(){
            console.log('\x1b[32m', 'sass rebuild success\n');
    });

    gulp.watch(['public/js/lib.jsx'], ['es6'])
        .on('change', function(){
            console.log('\x1b[32m', 'es6 rebuild success\n');
    });
});

gulp.task('css', function() {
    gulp.src('public/css/style.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('public/dest/css'));
});

gulp.task('server', function() {
    gulp.src('./')
        .pipe(server({
            host: '0.0.0.0',
            directoryListing: {enable: true},
            defaultFile: 'index.html',
            livereload: {
                enable: true,
                filter: function(filePath, cb) {
                    cb( !(/node_modules/.test(filePath)) );
                }
            }
        }))
});

gulp.task('es6', function(){
    gulp.src('public/js/lib.jsx')
        .pipe(babel({ blacklist: ["useStrict"] }))
        .pipe(rename('lib.es5.js'))
        .pipe(gulp.dest('public/dest/js'));
});

gulp.task('dest', function(){

    let now = Date.now();

    // 编译scss
    gulp.src('public/css/style.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('dest/public/css/'));

    // 移动图标的 css
    gulp.src('public/css/ionicons.min.css')
        .pipe(gulp.dest('dest/public/css'));

    // 编译es6
    gulp.src('public/js/lib.jsx')
        .pipe(babel({ blacklist: ["useStrict"] }))
        .pipe(rename('lib.js'))
        .pipe(uglify({mangle: true}))
        .pipe(gulp.dest('dest/public/js/'));

    // 合并压缩依赖
    gulp.src(['public/js/lib/react.min.js', 'public/js/lib/react-dom.min.js', 'public/js/lib/zepto.min.js'])
        .pipe(concat('lib-dependon.js'))
        .pipe(uglify({mangle: true}))
        .pipe(gulp.dest('dest/public/js/'));

    // 移动图标font
    gulp.src('public/font/*')
        .pipe(gulp.dest('dest/public/font'));

    // 移动背景图片
    gulp.src('public/img/*')
        .pipe(gulp.dest('dest/public/img'));

    // 移动html,改变路径
    gulp.src('index.html')
        .pipe(htmlreplace({
            'dev_dependon' : `public/js/lib-dependon.js?v=${now}`,
            'dev_dest': `public/js/lib.js?v=${now}`,
            'dev_css': `public/css/style.css?v=${now}`
        }))
        .pipe(rename('index.html'))
        .pipe(gulp.dest('dest'));           
});
 
